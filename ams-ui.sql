-- MySQL dump 10.13  Distrib 5.7.18, for Win64 (x86_64)
--
-- Host: localhost    Database: attendence_ms
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actual_attendence`
--

DROP TABLE IF EXISTS `actual_attendence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actual_attendence` (
  `Attendence_id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `hours` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`Attendence_id`),
  KEY `fk_user_id_idx` (`user_id`),
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `profile` (`profile_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=202 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actual_attendence`
--

LOCK TABLES `actual_attendence` WRITE;
/*!40000 ALTER TABLE `actual_attendence` DISABLE KEYS */;
INSERT INTO `actual_attendence` VALUES (32,'2017-06-26',9,1,0),(33,'2017-06-27',9,1,0),(34,'2017-06-28',9,1,0),(35,'2017-06-29',9,1,0),(36,'2017-06-30',9,1,0),(37,'2017-07-01',9,1,0),(38,'2017-07-02',9,1,0),(39,'2017-07-03',9,1,0),(40,'2017-07-04',9,1,0),(41,'2017-07-05',9,1,0),(42,'2017-07-06',9,1,0),(43,'2017-07-07',9,1,0),(44,'2017-07-08',9,1,0),(45,'2017-07-09',9,1,0),(46,'2017-07-10',9,1,0),(47,'2017-07-11',9,1,0),(49,'2017-07-12',9,1,0),(50,'2017-07-13',9,1,0),(51,'2017-07-14',9,1,0),(52,'2017-07-15',9,1,0),(53,'2017-07-16',9,1,0),(54,'2017-07-17',9,1,0),(55,'2017-07-18',9,1,0),(56,'2017-07-19',9,1,0),(57,'2017-07-20',9,1,0),(58,'2017-07-21',9,1,0),(59,'2017-07-22',9,1,0),(60,'2017-07-23',9,1,0),(61,'2017-07-24',9,1,0),(62,'2017-07-25',9,1,0),(63,'2017-07-26',9,1,0),(64,'2017-07-27',9,1,0),(65,'2017-07-28',9,1,0),(66,'2017-07-29',9,1,0),(67,'2017-07-30',9,1,0),(69,'2017-07-31',9,1,0),(70,'2017-08-01',9,1,0),(71,'2017-08-02',9,1,0),(72,'2017-08-03',9,1,0),(73,'2017-08-04',9,1,0),(74,'2017-08-05',9,1,0),(75,'2017-08-06',9,1,0),(76,'2017-08-07',9,1,0),(77,'2017-08-08',9,1,0),(78,'2017-08-09',9,1,0),(79,'2017-08-10',9,1,0),(80,'2017-08-11',9,1,0),(81,'2017-08-12',9,1,0),(82,'2017-08-13',9,1,0),(83,'2017-08-14',9,1,0),(84,'2017-08-15',9,1,0),(85,'2017-08-16',9,1,0),(86,'2017-08-17',9,1,0),(87,'2017-08-18',9,1,0),(88,'2017-08-19',0,1,0),(89,'2017-08-20',0,1,0),(90,'2017-06-26',9,3,0),(91,'2017-06-27',9,3,0),(92,'2017-06-28',9,3,0),(93,'2017-06-29',9,3,0),(94,'2017-06-30',9,3,0),(95,'2017-07-01',9,3,0),(96,'2017-07-02',9,3,0),(97,'2017-07-03',9,3,0),(98,'2017-07-04',9,3,0),(99,'2017-07-05',9,3,0),(100,'2017-07-06',9,3,0),(101,'2017-07-07',9,3,0),(102,'2017-07-08',0,3,0),(103,'2017-07-09',0,3,0),(104,'2017-07-10',9,3,0),(105,'2017-07-11',9,3,0),(106,'2017-07-12',9,3,0),(107,'2017-07-13',9,3,0),(108,'2017-07-14',9,3,0),(109,'2017-07-15',0,3,0),(110,'2017-07-16',0,3,0),(111,'2017-07-17',9,3,0),(112,'2017-07-18',9,3,0),(113,'2017-07-19',9,3,0),(114,'2017-07-20',9,3,0),(115,'2017-07-21',9,3,0),(116,'2017-07-22',0,3,0),(117,'2017-07-23',0,3,0),(118,'2017-07-24',9,3,0),(119,'2017-07-25',9,3,0),(120,'2017-07-26',9,3,0),(121,'2017-07-27',9,3,0),(122,'2017-07-28',9,3,0),(123,'2017-07-29',0,3,0),(124,'2017-07-30',0,3,0),(125,'2017-08-21',9,1,0),(126,'2017-08-22',9,1,0),(127,'2017-08-23',9,1,0),(128,'2017-08-24',9,1,0),(129,'2017-08-25',9,1,0),(130,'2017-08-26',9,1,0),(131,'2017-08-27',9,1,0),(132,'2017-08-28',9,1,0),(133,'2017-08-29',9,1,0),(134,'2017-08-30',9,1,0),(135,'2017-08-31',9,1,0),(136,'2017-09-01',9,1,0),(137,'2017-09-02',9,1,0),(138,'2017-09-03',9,1,0),(139,'2017-07-31',9,3,0),(140,'2017-08-01',9,3,0),(141,'2017-08-02',9,3,0),(142,'2017-08-03',9,3,0),(143,'2017-08-04',9,3,0),(144,'2017-08-05',9,3,0),(145,'2017-08-06',9,3,0),(146,'2017-08-07',9,3,0),(147,'2017-08-08',9,3,0),(148,'2017-08-09',9,3,0),(149,'2017-08-10',9,3,0),(150,'2017-08-11',9,3,0),(151,'2017-08-12',9,3,0),(152,'2017-08-13',9,3,0),(153,'2017-09-04',9,1,0),(154,'2017-09-05',9,1,0),(155,'2017-09-06',9,1,0),(156,'2017-09-07',9,1,0),(157,'2017-09-08',9,1,0),(158,'2017-09-09',9,1,0),(159,'2017-09-10',9,1,0),(160,'2017-09-11',9,1,0),(161,'2017-09-12',9,1,0),(162,'2017-09-13',9,1,0),(163,'2017-09-14',9,1,0),(164,'2017-09-15',9,1,0),(165,'2017-09-16',9,1,0),(166,'2017-09-17',9,1,0),(167,'2017-08-14',9,3,1),(168,'2017-08-15',9,3,1),(169,'2017-08-16',9,3,1),(170,'2017-08-17',9,3,1),(171,'2017-08-18',9,3,1),(172,'2017-08-19',9,3,1),(173,'2017-08-20',9,3,1),(174,'2017-08-21',9,3,1),(175,'2017-08-22',9,3,1),(176,'2017-08-23',9,3,1),(177,'2017-08-24',9,3,1),(178,'2017-08-25',9,3,1),(179,'2017-08-26',9,3,1),(180,'2017-08-27',9,3,1),(181,'2017-09-18',9,1,1),(182,'2017-09-19',9,1,1),(183,'2017-09-20',9,1,1),(184,'2017-09-21',9,1,1),(185,'2017-09-22',9,1,1),(186,'2017-09-23',9,1,1),(187,'2017-09-24',9,1,1),(188,'2017-09-25',9,1,1),(189,'2017-09-26',9,1,1),(190,'2017-09-27',9,1,1),(191,'2017-09-28',9,1,1),(192,'2017-09-29',9,1,1),(193,'2017-09-30',9,1,1),(194,'2017-10-01',9,1,1),(195,'2017-10-02',9,1,0),(196,'2017-10-03',9,1,0),(197,'2017-10-04',9,1,0),(198,'2017-10-05',9,1,0),(199,'2017-10-06',9,1,0),(200,'2017-10-07',9,1,0),(201,'2017-10-08',9,1,0);
/*!40000 ALTER TABLE `actual_attendence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attendance`
--

DROP TABLE IF EXISTS `attendance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attendance` (
  `monday` varchar(45) NOT NULL,
  `tuesday` varchar(45) DEFAULT NULL,
  `wednesday` varchar(45) DEFAULT NULL,
  `thursday` varchar(45) DEFAULT NULL,
  `friday` varchar(45) DEFAULT NULL,
  `saturday` varchar(45) DEFAULT NULL,
  `sunday` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`monday`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attendance`
--

LOCK TABLES `attendance` WRITE;
/*!40000 ALTER TABLE `attendance` DISABLE KEYS */;
INSERT INTO `attendance` VALUES ('9','9','9','9','9','0','0');
/*!40000 ALTER TABLE `attendance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login` (
  `login_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_type` varchar(45) DEFAULT NULL,
  `manger_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`login_id`),
  KEY `fk_profile_id_idx` (`user_id`,`manger_id`),
  KEY `fk_manager_id_idx` (`manger_id`),
  CONSTRAINT `fk_manager_id` FOREIGN KEY (`manger_id`) REFERENCES `profile` (`profile_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_profile_id` FOREIGN KEY (`user_id`) REFERENCES `profile` (`profile_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login`
--

LOCK TABLES `login` WRITE;
/*!40000 ALTER TABLE `login` DISABLE KEYS */;
INSERT INTO `login` VALUES (1,'emp1','corp@123',1,'s',2),(2,'manager1','corp@123',2,'m',7),(3,'emp2','corp@123',3,'s',2),(4,'emp3','corp@123',4,'s',6),(5,'emp4','corp@123',5,'s',6),(6,'emp5','corp@123',6,'m',7);
/*!40000 ALTER TABLE `login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile` (
  `profile_id` int(11) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `mail_id` varchar(45) DEFAULT NULL,
  `designation` varchar(45) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  PRIMARY KEY (`profile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile`
--

LOCK TABLES `profile` WRITE;
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
INSERT INTO `profile` VALUES (1,'emp1',23,'emp1@gmail.com','Software Engineer','1993-04-05'),(2,'manager',35,'manager@gmail.com','Manager','1947-04-05'),(3,'emp2',22,'emp2@gmail.com','Softwareengineer','1993-05-05'),(4,'emp3',23,'emp3@gmail.com','SoftEngineer','1993-04-02'),(5,'emp4',23,'emp4@123.com','Consultant','1993-07-05'),(6,'emp5',28,'emp5@abc.ccom','consultant','1987-07-04'),(7,'default',30,'default@default.com','default','1999-01-01');
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session` (
  `session_id` int(11) NOT NULL AUTO_INCREMENT,
  `current_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `session`
--

LOCK TABLES `session` WRITE;
/*!40000 ALTER TABLE `session` DISABLE KEYS */;
INSERT INTO `session` VALUES (2,'2');
/*!40000 ALTER TABLE `session` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-02 11:39:20
