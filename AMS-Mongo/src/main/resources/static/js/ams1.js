function getApprovalsList(){
	
	$("#Pending tr").remove();
	$.ajax({
	 url : "/getApprovalList",
	 type: "GET",
	 success : function(data){
		
		 var tr=[];
	  		
	  		tr.push('<tr style="color:#1e81d6;">');
			tr.push("<th>Employee Id</th>");
			tr.push("<th>Employee Name</th>");
			tr.push("<th>Pending</th>");
			tr.push('</tr>');
			
			
			
			for(var i=0;i<data.length;i++){
				tr.push('<tr style="color:#1e81d6;">');
	  			tr.push("<td>"+data[i].userId+"</td>");
	  			tr.push("<td>"+data[i].userName+"</td>");
	  			tr.push("<th><input type='button' value='Pending'  style='margin:8%;' class='btn btn-primary ' onclick='Pending("+data[i].userId+")' /></th>");
	  			tr.push('</tr>');
			}
	  			
		 
		 
	  			$('table[id=Pending]').append($(tr.join('')));
	 },
	 error:function(data){
		 alert("error"+data)
	 }
	})
}

function Pending(userId){
	$("#Pending tr").remove();
	
	$.ajax({
	 url : "/getPendingList",
	 data:{
		 "userId" :userId
	 },
	 type: "GET",
	 success : function(data){
	 
	 
	 var tr=[];
		
		tr.push('<tr style="color:#1e81d6;">');
		tr.push("<th>From date</th>");
		tr.push("<th>To Date</th>");
		tr.push("<th>Approved</th>");
		tr.push('</tr>');
		
	
		
		
		
		for(var i=0;i<data.length;i=i+7){
			tr.push('<tr style="color:#1e81d6;">');
			tr.push("<td>"+data[i].date+"</td>");
		var j=i+6
			tr.push("<td>"+data[j].date+"</td>");
			tr.push("<th><input type='button' value='Pending'  style='margin:8%;' class='btn btn-primary ' onclick='Approved("+data[i].attendenceId+")' /></th>");
			tr.push('</tr>');
		}
			
	 
	 
			$('table[id=Pending]').append($(tr.join('')));
	 
	 
	 
	 
	 
	 
	 
	 }
	});
	 }




function Approved(attendenceId){
	$.ajax({
		 url : "/Approved",
		 data: {"attendenceId" :attendenceId },
		 type: "GET",
		 success : function(data){
		
		 Pending(data)
		 }
	});
	
}
