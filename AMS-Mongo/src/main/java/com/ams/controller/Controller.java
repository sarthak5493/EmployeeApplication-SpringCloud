package com.ams.controller;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ams.entity.Attendence;
import com.ams.entity.UserLogin;
import com.ams.service.AmsService;



@RestController
public class Controller {
	
	
	@Autowired
	private AmsService service;
	
	
	
	
	@RequestMapping(value="getApprovalList", method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public List<UserLogin> getApprovalsList(){

		return service.getApprovalList(2);
		
	}
	
	
	@RequestMapping(value="getPendingList", method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Attendence> getPendingList(@RequestParam("userId") int userId ){


		return service.getPendingList(userId);	
	}
	
	
	@RequestMapping(value="Approved", method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public int ApprovedUpdated(@RequestParam("attendenceId") int attendenceId ){
		int i=service.updateApproval(attendenceId); 
		
		

		return service.getUserId(attendenceId).getUserId();


		 
		
	}
	
	
	
	

}
