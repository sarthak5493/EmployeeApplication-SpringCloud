package com.ams.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.ams.entity.Attendence;

public interface AttendenceRepo extends CrudRepository<Attendence, Serializable>{

	
	@Query("SELECT attendence as attendence FROM Attendence attendence WHERE attendence.status=:status")
	public List<Attendence> getApprovalPending(@Param("status") int status);
	
	
	
	@Query("SELECT attendence as attendence FROM Attendence attendence WHERE attendence.userId=:userId AND attendence.status=:status")
	public List<Attendence> getPendingList(@Param("status") int status,@Param("userId") int userId);

	
	@Modifying
	@Transactional
	@Query("UPDATE Attendence a SET a.status=?1 WHERE a.attendenceId=?2")
	public int updateApproval(@Param("status") int status,@Param("attendenceId") int attendenceId);
	
	
	
	public Attendence findByAttendenceId(int attendenceId);
	
	
	
	
}
