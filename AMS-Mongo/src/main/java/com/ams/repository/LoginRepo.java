package com.ams.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ams.entity.UserLogin;


public interface LoginRepo extends CrudRepository<UserLogin, Serializable> {
	
	
	
	
	@Query("SELECT user as user FROM UserLogin user WHERE user.managerId=:Id")
	public List<UserLogin> getApprovalList(@Param("Id") int Id);

}
