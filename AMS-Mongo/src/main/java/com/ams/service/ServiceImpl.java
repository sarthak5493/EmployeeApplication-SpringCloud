package com.ams.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.ams.entity.Attendence;
import com.ams.entity.UserLogin;
import com.ams.repository.AttendenceRepo;
import com.ams.repository.LoginRepo;



@org.springframework.stereotype.Service
public class ServiceImpl implements AmsService{
	
	
	@Autowired
	private LoginRepo LoginRepo;
	
	@Autowired
	private AttendenceRepo attendenceRepo;

	@Override
	public List<UserLogin> getApprovalList(int Id) {
		// TODO Auto-generated method stub
		List<UserLogin> login=LoginRepo.getApprovalList(Id);
		List<Attendence> temp=attendenceRepo.getApprovalPending(1);
		
		
		
		
		
		List<UserLogin> actualPending=new ArrayList<UserLogin>();
		
		for(UserLogin i:login){
				for(Attendence j:temp){
					if(j.getStatus() == 1){
					if(i.getUserId() == j.getUserId()){
						int check=0;
						for(UserLogin k:actualPending){
							if(k.getUserId() == i.getUserId()){
								check=1;
							}
						}
						if(check == 0){
						UserLogin x=new UserLogin();
						x.setUserName(i.getUserName());
						x.setUserId(i.getUserId());
						actualPending.add(x);
						}
					}
					}
				}
			 
			
			
		}
		
		
		return actualPending;
	}

	@Override
	public List<Attendence> getPendingList(int userId) {
		// TODO Auto-generated method stub
		return attendenceRepo.getPendingList(1, userId);
	}

	@Override
	public int updateApproval(int attendenceId) {
		// TODO Auto-generated method stub
		
		for(int i=attendenceId;i<attendenceId+7;i++){
		int  temp=attendenceRepo.updateApproval(0, i);
		}
		
		
		return 1;
	}

	@Override
	public Attendence getUserId(int attendenceId) {
		// TODO Auto-generated method stub
		return attendenceRepo.findByAttendenceId(attendenceId);
	}

}
