package com.ams.service;

import java.util.List;

import com.ams.entity.Attendence;
import com.ams.entity.UserLogin;



public interface AmsService {
	

	
	public List<UserLogin> getApprovalList(int Id);
	
	public List<Attendence> getPendingList(int userId);
	
	
	public int updateApproval(int attendenceId);
	
	public Attendence getUserId(int attendenceId);
	
	

}
