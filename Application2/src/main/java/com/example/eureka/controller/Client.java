package com.example.eureka.controller;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("APPLICATION1")
public interface Client {
	
	@RequestMapping(value="/hello",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public String getHello();

}
