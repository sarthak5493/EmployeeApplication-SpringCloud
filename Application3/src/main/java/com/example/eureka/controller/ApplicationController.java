package com.example.eureka.controller;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.http.HttpEntity;



import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


@RestController
public class ApplicationController {
	
	@Autowired
	LoadBalancerClient loadBalancer;
	
	@GetMapping("consume")
	public ResponseEntity<String> getHello(){
		
		
		
		   
		   ServiceInstance serviceInstance=loadBalancer.choose("APPLICATION1");
		   
			String baseUrl = serviceInstance.getUri().toString();
			baseUrl=baseUrl+"/hello";
			
			
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> response=null;
			response=restTemplate.exchange(baseUrl,HttpMethod.GET, getHeaders(),String.class);
			System.out.println(response.getBody());
			
			return response;
		}
		
		

		private HttpEntity getHeaders() {
			HttpHeaders headers = new HttpHeaders();
			headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
			return new HttpEntity<>(headers);
			
		}

}
