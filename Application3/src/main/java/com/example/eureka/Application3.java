package com.example.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import com.example.eureka.controller.ApplicationController;

@SpringBootApplication
@EnableEurekaClient
public class Application3 {

	public static void main(String[] args) {
		
		ApplicationContext ctx= SpringApplication.run(Application3.class, args);
		
	}
	
	
	
}
