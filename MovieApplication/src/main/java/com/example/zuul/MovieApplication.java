package com.example.zuul;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class MovieApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieApplication.class, args);
	}
	 @RequestMapping(value = "/movies")
	    public List<String> available() {
	        List<String> movies = new ArrayList<>();
	        movies.add("Movie1 (1984)");
	        movies.add("Movie2 (2016)");
	        return movies;
	    }
}
