package com.ams.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="actual_attendence")
public class Attendence {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="Attendence_id")
	private int attendenceId;
	
	@Column(name="date")
	private Date date;
	
	@Column(name="hours")
	private int hours;
	
	
	@Column(name="status")
	private int status;
	
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name="	user_id")
	private int userId;

	public int getAttendenceId() {
		return attendenceId;
	}

	public void setAttendenceId(int attendenceId) {
		this.attendenceId = attendenceId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		this.hours = hours;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "Attendence [attendenceId=" + attendenceId + ", date=" + date + ", hours=" + hours + ", userId=" + userId
				+ "]";
	}
	

	
	
	

}
