package com.ams.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="session")
public class Session {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="session_id")
	private int sessionId;
	

	@Column(name="current_id")
	private int userId;


	public int getSessionId() {
		return sessionId;
	}


	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}


	public int getUserId() {
		return userId;
	}


	public void setUserId(int userId) {
		this.userId = userId;
	}


	@Override
	public String toString() {
		return "session [sessionId=" + sessionId + ", userId=" + userId + "]";
	}
	

	
	
	
	
}
