package com.ams.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.ams.entity.AMSLogin;

public interface LoginRepo extends CrudRepository<AMSLogin, Serializable> {

	
	public AMSLogin findByUserName(String userName);
	
	
	
	
	
	
	
}
