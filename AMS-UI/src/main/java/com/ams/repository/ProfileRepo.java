package com.ams.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.ams.entity.Profile;

public interface ProfileRepo extends CrudRepository<Profile, Serializable>{
	
	@Query("SELECT profile as profile FROM Profile profile WHERE profile.profileId=:profileId")
	public Profile getProfile(@Param("profileId") int profileId);

}
