package com.ams.repository;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.ams.entity.Attendence;

public interface AttendenceRepo extends CrudRepository<Attendence, Serializable> {

	@Query("SELECT attendence as attendence FROM Attendence attendence WHERE attendence.userId=:userId")
	public List<Attendence> getAttendenceById(@Param("userId") int userId);
	
	
	
	
	@Query("SELECT attendence as attendence FROM Attendence attendence WHERE attendence.userId=:userId AND attendence.date BETWEEN :StartOfWeek AND :EndOfWeek)")
	public List<Attendence> getAttendenceByDateRange(@Param("userId") int userId, @Param("StartOfWeek") Date StartOfWeek,@Param("EndOfWeek") Date EndOfWeek);
	
	
	
	
}
