package com.ams.controller;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ws.rs.Consumes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ams.entity.AMSLogin;
import com.ams.entity.Attendence;
import com.ams.entity.Profile;
import com.ams.entity.Session;
import com.ams.entity.check;
import com.ams.service.AmsService;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSInputFile;


@RestController
@RefreshScope
public class AMSController {
	
	@Autowired
	private AmsService amsService;
	
	@Value("${message}")
	private String message;
	
	
	@RequestMapping("/message")
	public String getMessage(){
		return this.message;
	}

	@RequestMapping(value="loginValidate", method=RequestMethod.GET,produces=MediaType.TEXT_HTML_VALUE)
	public String validateUser(@RequestParam("userName") String userName,@RequestParam("password") String password){
		
		 AMSLogin test=amsService.validateLogin(userName);
			Session sessiondata=new Session(); 
			sessiondata.setSessionId(2);
		if(test.getPassword().equals(password)){

			sessiondata.setUserId(test.getUserId());
			amsService.updateSession(sessiondata);
			
			if(test.getType().equalsIgnoreCase("s")){
				return "menu";
			}else 	if(test.getType().equalsIgnoreCase("m")){
				return "admin";
			}else{
				return "error";
			}
			
			
			
		}else {

			sessiondata.setUserId(0);
			amsService.updateSession(sessiondata);
			
			return "index";
		}
		
		
	}
	
	
	@RequestMapping(value="getAttendence", method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Attendence> getAttendence(){
		Session temp=amsService.getUserId(2);
		return amsService.getcurrentAttendence(temp.getUserId());
	}
	
	@RequestMapping(value="previous", method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Attendence> getPrevious(@RequestParam("startDate") Date startDate){
		Session temp=amsService.getUserId(2);
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(startDate);
		
		cal.add(Calendar.DATE, -1);
		
		Date EndOfWeek = new java.sql.Date(cal.getTime().getTime());
		cal.add(Calendar.DATE, -6);
		
		
		Date StartOfWeek = new java.sql.Date(cal.getTime().getTime());
		
		
		
		return amsService.getAttendenceByWeek(temp.getUserId(), StartOfWeek, EndOfWeek);
	}
	
	@RequestMapping(value="next", method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Attendence> getNext(@RequestParam("startDate") Date startDate){
		Session temp=amsService.getUserId(2);
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(startDate);
		cal.add(Calendar.DATE, 1);
		
	
		
		Date StartOfWeek = new java.sql.Date(cal.getTime().getTime());
		cal.add(Calendar.DATE, 6);
		
		
		Date EndOfWeek = new java.sql.Date(cal.getTime().getTime());	
		
	
		
		return amsService.getAttendenceByWeek(temp.getUserId(), StartOfWeek, EndOfWeek);
		
		
	}
	
	@RequestMapping(value="profile" , method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public Profile getProfile(){
		Session temp=amsService.getUserId(2);
		
		return amsService.getProfile(temp.getUserId());
	}
	
	
	
	@RequestMapping(value="ManualAttendence" , method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Attendence> getManualAttendence(){
		Session session=amsService.getUserId(2);
		List<Attendence> temp=amsService.getAllAttendence(session.getUserId());
		
		Date last_date=null;
 		
		for(Attendence i:temp){
			last_date=i.getDate();
		}
		

		List<Attendence> weekOfDate = new ArrayList<Attendence>();
		
		
		for(int i=1;i<8;i++){
		Attendence check=new Attendence();
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(last_date);
			cal.add(Calendar.DATE, i);
			check.setDate(new java.sql.Date(cal.getTime().getTime()));
			
			weekOfDate.add(check);
		}
		

		
		
		return weekOfDate; 
	}

	
	
	@RequestMapping(value="GetManualAttendence" , method=RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE)
	public int GetManualAttendence(@RequestBody List<check> DataJson){
		Session temp=amsService.getUserId(2);

		
		for(check i: DataJson){
			Attendence Update=new Attendence();
			Update.setDate(i.getDate());
			Update.setHours(i.getHours());
			Update.setUserId(temp.getUserId());
			Update.setUserId(temp.getUserId());
			Update.setStatus(1);
			amsService.ManualAttendence(Update);
			
		}
			
		return 1;
	}
	
/*	
	@RequestMapping(value="/fileUpload",method=RequestMethod.POST,consumes=javax.ws.rs.core.MediaType.MULTIPART_FORM_DATA)
	public String upload(@RequestParam("file") MultipartFile file) throws IOException{
		MongoClient mongo=new MongoClient("localhost", 27017);
		DB mongoDB =mongo.getDB("Attendance");
		GridFS fileStore =new GridFS(mongoDB,"fs");
		GridFSInputFile gridInputFile=fileStore.createFile(file.getInputStream());
		gridInputFile.setFilename("sarthak");
		gridInputFile.save();
		
		return "working";
	}*/

	
	
}
