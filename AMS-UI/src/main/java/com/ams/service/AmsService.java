package com.ams.service;

import java.sql.Date;
import java.util.List;

import org.springframework.data.repository.query.Param;

import com.ams.entity.AMSLogin;
import com.ams.entity.Attendence;
import com.ams.entity.Profile;
import com.ams.entity.Session;

public interface AmsService {
	
	public AMSLogin validateLogin(String userName);
	
	public int updateSession(Session sessiondata);
	
	public Session getUserId(int sessionId);
	
	public List<Attendence> getcurrentAttendence(int userId);
	
	public List<Attendence> getAttendenceByWeek(int userId,Date StartOfWeek, Date EndOfWeek);
	
	public Profile getProfile(int profileId);
	
	public List<Attendence> getAllAttendence(int userId);
	
	public Attendence ManualAttendence(Attendence update);
	
	
}
