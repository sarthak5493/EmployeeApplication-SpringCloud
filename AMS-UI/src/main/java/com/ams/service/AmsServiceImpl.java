package com.ams.service;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ams.entity.AMSLogin;
import com.ams.entity.Attendence;
import com.ams.entity.Profile;
import com.ams.entity.Session;
import com.ams.repository.AttendenceRepo;
import com.ams.repository.LoginRepo;
import com.ams.repository.ProfileRepo;
import com.ams.repository.sessionRepo;

@Service
public class AmsServiceImpl implements AmsService{
	
	
	@Autowired
	private LoginRepo loginRepo;

	@Autowired
	private sessionRepo sessionRepo;
	
	@Autowired
	private AttendenceRepo attendenceRepo;
	
	@Autowired
	private ProfileRepo profileRepo;
	
	
	@Override
	public AMSLogin validateLogin(String userName) {
		// TODO Auto-generated method stub
		return loginRepo.findByUserName(userName);
	}

	@Override
	public int updateSession(Session sessiondata) {
		// TODO Auto-generated method stub
		sessionRepo.save(sessiondata);
		return 1;
	}

	@Override
	public Session getUserId(int sessionId) {
		// TODO Auto-generated method stub
		return sessionRepo.findOne(sessionId);
	}

	@Override
	public List<Attendence> getcurrentAttendence(int userId) {
		Calendar temp=null;
		Date StartOfWeek = null;
		Date EndOfWeek = null;
		Calendar cal = Calendar.getInstance();
		/*System.out.println("cal"+cal);
		System.out.println("cal.get(cal.DAY_OF_WEEK)"+cal.get(cal.DAY_OF_WEEK));
		System.out.println("Calendar.DATE"+Calendar.DATE);*/
		
		switch (cal.get(cal.DAY_OF_WEEK)) {
		
		case 1:
			cal.add(Calendar.DATE, -6);
			temp=cal;
			StartOfWeek=new java.sql.Date(cal.getTime().getTime());
			
			break;
		case 2:
			StartOfWeek=new java.sql.Date(cal.getTime().getTime());
			temp=cal;
			break;
		case 3:
			cal.add(Calendar.DATE, -1);
			StartOfWeek=new java.sql.Date(cal.getTime().getTime());
			temp=cal;
			break;
		case 4:
			cal.add(Calendar.DATE, -2);
			StartOfWeek=new java.sql.Date(cal.getTime().getTime());
			temp=cal;
			break;
			
		case 5:
			cal.add(Calendar.DATE, -3);
			temp=cal;
			StartOfWeek=new java.sql.Date(cal.getTime().getTime());

			break;
		case 6:
			cal.add(Calendar.DATE, -4);
			StartOfWeek=new java.sql.Date(cal.getTime().getTime());
			temp=cal;
			break;
		case 7:
			cal.add(Calendar.DATE, -5);
			StartOfWeek=new java.sql.Date(cal.getTime().getTime());
			temp=cal;
			break;

		default:
			break;
		}
		
		temp.add(Calendar.DATE, 6);
		EndOfWeek=new java.sql.Date(temp.getTime().getTime());
		/*System.out.println("temp"+temp);
		System.out.println("StartOfWeek"+StartOfWeek);
		System.out.println("EndOfWeek"+EndOfWeek);*/


		return attendenceRepo.getAttendenceByDateRange(userId, StartOfWeek, EndOfWeek);
	}

	@Override
	public List<Attendence> getAttendenceByWeek(int userId, Date StartOfWeek, Date EndOfWeek) {
		
		return attendenceRepo.getAttendenceByDateRange(userId, StartOfWeek, EndOfWeek);
	}

	@Override
	public Profile getProfile(int profileId) {
		
		return profileRepo.getProfile(profileId);
	}

	@Override
	public List<Attendence> getAllAttendence(int userId) {
		// TODO Auto-generated method stub
		return attendenceRepo.getAttendenceById(userId);
	}

	@Override
	public Attendence ManualAttendence(Attendence update) {
		// TODO Auto-generated method stub
		return attendenceRepo.save(update);
	}


	

}
